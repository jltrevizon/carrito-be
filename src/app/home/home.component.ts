import { Component, OnInit } from '@angular/core';
import { Category } from '../interfaces/category';
import CategoriesJson from '../services/data/categories.json';
import ArticlesJson from '../services/data/articles.json';
import { Article } from '../interfaces/article';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})


export class HomeComponent implements OnInit {
  categories: Category[] = CategoriesJson;
  articles: Article[] = ArticlesJson;
  items: Article[] = [];
  constructor( ) { }

  ngOnInit(): void {
    this.items = this.articles;
  }
  filterCategory(e: Event, item?: Category){
    e.preventDefault();
    e.stopPropagation();
    if (!item)
    this.items = this.articles;
    else
    this.items = this.articles.filter((el: Article) => el.category == item.name);
  }

}
