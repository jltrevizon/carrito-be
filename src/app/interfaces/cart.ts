import { Article } from "./article";

export interface Cart {
  article: Article
  count: number
}
