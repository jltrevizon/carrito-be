import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cart } from '../interfaces/cart';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  cart  : Cart[] = [];
  constructor(private router: Router,
    private cartService: CartService) { }

  ngOnInit(): void {
    this.load();
  }
  load(){
    this.cart = this.cartService.all();
  }
  submit(){
    this.router.navigate(['/checkout']);
  }
  addItem(cart: Cart, quantity:number){
    let item = this.cart.find(el=>el.article.code == cart.article.code);
    if (!!item){
      item.count = item.count+quantity;
      this.cartService.save();
    }
    this.load();

  }
  removeItem(e: Event, cart: Cart){
    e.preventDefault();
    this.cartService.remove(cart);
    this.load();
  }

}
