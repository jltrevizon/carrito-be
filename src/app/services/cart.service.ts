import { Injectable } from '@angular/core';
import { Article } from '../interfaces/article';
import { Cart } from '../interfaces/cart';
const KEY_STORAGE = 'cart-key';

@Injectable({
  providedIn: 'root'
})

export class CartService {
  list : Cart[];
  constructor() {
    this.init();
  }

  public init(){
    if(!localStorage.getItem(KEY_STORAGE)){
      localStorage.setItem(KEY_STORAGE, '[]');
    }
  }
  public add(item: Article, count: number) {

    this.list = this.get();
    let index = this.list.findIndex(el=>el.article.code == item.code);
    if (index===-1){
      let cart = {} as Cart;
      cart.article = item;
      cart.count = count;
      this.list.push(cart);
    } else {
      let cart = this.list[index];
      cart.count = cart.count + count;
    }

    this.save();
  }

  public find(item: Article) {
    this.all();
    return this.list.find(el=>el.article.code == item.code);
  }

  private get() {
    return JSON.parse(localStorage.getItem(KEY_STORAGE) || '[]');
  }
  public all() {
    this.list = this.get();
    return this.list;
  }
  public remove(item: Cart) {
    this.all();
    this.list = this.list.filter(el=>item.article.code != el.article.code);
    this.save();
  }

  public save(){
    this.list = this.list.filter(el=>el.count != 0);
    localStorage.setItem(KEY_STORAGE, JSON.stringify(this.list));
  }

  public clear() {
    localStorage.clear();
  }
}
