import { Component, Input, OnInit } from '@angular/core';
import { Article } from '../interfaces/article';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  @Input() item : Article;
  constructor(private cartService: CartService) { }

  ngOnInit(): void {
  }
  agregar(item: Article){
    this.cartService.add(item, 1);
  }
  getCount(item: Article){
    let article = this.cartService.find(item);
    return article?.count || 0;
  }
}
