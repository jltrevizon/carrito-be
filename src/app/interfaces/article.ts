export interface Article {
  name: string,
  code: string,
  description: string,
  discount: number,
  price: number,
  stock: number,
  image: string,
  category: string
}
