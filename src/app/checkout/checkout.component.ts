import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cart } from '../interfaces/cart';
import { CartService } from '../services/cart.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  cart  : Cart[] = [];
  total : string =  '';
  tasa : number = 5;
  subtotal : string =  '';
  descuento : string =  '';
  impuesto : string =  '';
  constructor(private cartService: CartService,
    private router: Router) { }

  ngOnInit(): void {
    this.load();
  }
  load(){
    this.cart = this.cartService.all();
    let subtotal = 0;
    let descuento = 0;
    let impuesto = 0;
    this.cart.map(el=>{
      subtotal += el.count * el.article.price;
      descuento += el.count * el.article.discount;
    });
    impuesto = (subtotal - descuento) * (this.tasa/100);
    this.subtotal = subtotal.toFixed(2);
    this.descuento = descuento.toFixed(2);
    this.impuesto  = impuesto.toFixed(2);
    this.total = (subtotal - descuento + impuesto).toFixed(2);
  }

  pagar(){
    this.cartService.clear();
    alert('Gracias por su compra!');
    this.router.navigate(['/home']);
  }

}
